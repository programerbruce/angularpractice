import { Injectable } from '@angular/core';
import { IEvent } from './events';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  private _url: string = "/assets/data/events.json";
  public API_URL: string = "http://api.openweathermap.org/data/2.5/weather?q=";
  private API_KEY: string = "45365278ec7a9ba753f1e4f7244a6072"
  public events;

  constructor(private http:HttpClient) {

  }

  getEvents(){
    if(!this.events){
      let result = this.http.get(this._url).catch(this.errorHandler);
      result.subscribe(data => {
        this.setEvents(data);
      });
      return result;
    }
    return this.events;

  }

  setEvents(events){
    this.events = events;
  }

  updateEvent(data){
    this.events.map(event => {
      event = data;
    })
  }

  addEvent(event){
    console.log('length of events:', this.events)
    let lstData = this.events[this.events.length - 1];
    console.log('lst one:', lstData);
    event.id = lstData.id + 1;
    this.events.push(event);
  }

  getEventTemperature(event) {
    const target_url = `${this.API_URL}${event.city},${event.country}&appid=${this.API_KEY}&units=metric`;
    console.log('target_url:', target_url);
    return this.http.get(target_url).catch(this.errorHandler);
  }

  errorHandler(error: HttpErrorResponse){
    return Observable.throw(error.message || "Server Error");
  }

}
