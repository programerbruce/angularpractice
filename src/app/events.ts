export interface IEvent {
    id: number;
    name: string;
    time: string;
    city: string;
    temperature: string;
    country: string;
}
