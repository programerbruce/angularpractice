import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { EventsService } from '../events.service';
import _ from "lodash";
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.scss']
})
export class EventDetailComponent implements OnInit {

  public eventId;
  public selectedEvent;
  public selectedEventName;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private _eventsService: EventsService) { }

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get('id'));
      this.eventId = id;
      console.log('id:', id);
      console.log("_eventService.events:",this._eventsService.events)
      console.log('_.find(this._eventsService.events, { id: id })[0]:', _.find(this._eventsService.events, { id: id }));
      this.selectedEvent = _.find(this._eventsService.events, { id: id });
      // this.selectedEvent.time = this.selectedEvent.time | date: 'yyyy-MM-dd HH:mm:ss';
      console.log('this.selectedEvent:', this.selectedEvent);
      this.selectedEventName = this.selectedEvent.name;
    });
  }

  updateEvent(){
    console.log('updateEvent - this.selectedEvent:', this.selectedEvent)
    this._eventsService.updateEvent(this.selectedEvent);
    console.log('updateEvent - this._eventsService.events:', this._eventsService.events);
    this.router.navigate(['/events']);
  }
  gotoEvents() {
    this.router.navigate(['/events']);
  }

}
