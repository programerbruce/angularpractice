import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap} from '@angular/router';
import { EventsService } from '../events.service';

@Component({
  selector: 'app-event-creation',
  templateUrl: './event-creation.component.html',
  styleUrls: ['./event-creation.component.scss']
})
export class EventCreationComponent implements OnInit {

  public event;

  constructor(private router: Router, private route: ActivatedRoute, private _eventsService: EventsService) {
    this.event = {
      name: '',
      time: '',
      city: '',
      country: '',
      temperature: ''
    };
  }

  ngOnInit() {

  }

  saveEvent(){
    console.log('saveEvent:', this.event);
    this._eventsService.addEvent(this.event);
    this.router.navigate(['/events']);
  }
  gotoEvents() {
    this.router.navigate(['/events']);
  }

}
