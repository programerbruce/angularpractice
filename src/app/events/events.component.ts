import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap} from '@angular/router';
import { EventsService } from '../events.service';
import _ from "lodash";

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss'],
})
export class EventsComponent implements OnInit {

  public selectedId;

  public selectedEvent;

  public events = [];
  public errorMsg;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private _eventsService: EventsService
  ) {}

  ngOnInit() {
    if(!this._eventsService.events){
      this._eventsService.getEvents()
      .subscribe(data => {
        console.log('data:', data);
        // this._eventsService.setEvents(data)
        this.events = this.events.concat(data);
        data.map(event => {
          this._eventsService.getEventTemperature(event)
          .subscribe(data => {
            event.temperature = data['main']['temp'];
          },error => this.errorMsg = error)
        })
      },
      error => this.errorMsg = error);
    }else{
      let data = this._eventsService.getEvents()
      this.events = this.events.concat(data);
      data.map(event => {
        this._eventsService.getEventTemperature(event)
        .subscribe(data => {
          event.temperature = data['main']['temp'];
        },error => this.errorMsg = error)
      })
    }

  }

  onSelect(event) {
    this.selectedEvent = event;
    this.router.navigate([event.id], {
     relativeTo: this.route
   }
   );
  }

  isSelected(event) { return event.id === this.selectedId; }

}
